export class user {
   cin:number;
	 nom:String ;
	 prenom:String;
	 date_naissance:Date;
	 email:String;
	 password:String;
	 typeU:String;

   constructor (cin:number, nom:String,prenom:String,date_naissance:Date,email:String,password:String,typeU:String) {
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.date_naissance = date_naissance;
		this.email = email;
		this.password = password;
		this.typeU = typeU;
  }
}
