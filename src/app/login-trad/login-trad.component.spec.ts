import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginTradComponent } from './login-trad.component';

describe('LoginTradComponent', () => {
  let component: LoginTradComponent;
  let fixture: ComponentFixture<LoginTradComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginTradComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginTradComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
